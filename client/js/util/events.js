define({
    ResultsLoadedEvent:'search-results-loaded',
    ResultsChangedEvent:'search-result-active-changed',

    HideSearch:'hide-search',
    ShowSearch:'show-search',

    HideLoader:'hide-loader',
    ShowLoader:'show-loader',

    HideDetails:'hide-details',
    ShowDetails:'show-details',

    SetNavEvent:'set-nav',
    RouteChange:'route-change'
});