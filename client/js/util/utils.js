define([

    ],
    function () {
        return {
            isLocalStorageSupported:function(){
                try{
                    return 'localStorage' in window && window['localStorage'] !== null;
                }catch(ex){
                    return false;
                }
            }
        };
    }
);