define([],
    function () {
        return {
            get: function (key) {
                var obj = undefined;
                try {
                    var str = localStorage.getItem(key);
                    obj = JSON.parse(str);
                } catch (ex){
                    console.log('error reading: '+key);
                }
                return obj;
            },
            save: function (key, value) {
                var str = JSON.stringify(value,null,' ');
                localStorage.setItem(key,str);
            },
            isLocalStorageSupported: function () {
                try {
                    return 'localStorage' in window && window['localStorage'] !== null;
                } catch (ex) {
                    return false;
                }
            }
        };
    }
);