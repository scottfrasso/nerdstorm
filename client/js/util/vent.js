define([

    ],
    function(){
        return Backbone.Model.extend({
            defaults:{
                bus:_.extend({},Backbone.Events)
            },
            initialize:function(){
                var bus = this.get('vent');
                if (typeof(bus) === 'undefined'){
                    bus = _.extend({},Backbone.Events);
                    this.set({
                        'bus':bus
                    })
                }
            }
        });
    });