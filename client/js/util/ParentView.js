
define(['jquery', 'underscore', 'backbone'],
    function ($, _, Backbone) {
        {
            var onCloseFunction = function () {
                this.remove();
                this.unbind();
                if (this.onClose) {
                    this.onClose();
                }
            };

            if (window.Backbone.View.prototype.close !== onCloseFunction) {
                window.Backbone.View.prototype.close = onCloseFunction;
            }
        }

        return Backbone.View.extend({
            childViews: {},
            onClose: function () {
                for (var childView in this.childViews) {
                    if (!this.childViews.hasOwnProperty(childView)) {
                        continue;
                    }
                    try {
                        this.childViews[childView].close();
                    } catch (ex) {
                        // could not close view
                    }
                }
            }
        });
    }
);