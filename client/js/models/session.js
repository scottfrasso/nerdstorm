define([
        'state',
        'landmark',
        'storage'
    ],
    function (State, LandmarkModel, Storage) {
        return Backbone.Model.extend({
            url: function () {
                return '/api/user';
            },
            idAttribute: '_id',
            defaults: {
                // Client side properties
                searchText:undefined,
                selectedLandmarkId: undefined,
                isLoggedIn: false,
                // Database properties
                user_id: undefined,
                name: undefined,
                google: {
                    id: undefined,
                    name: undefined,
                    email: undefined
                }
            },
            storageKey: 'nerd-storm-session',
            initialize: function () {
                if (this.get('google').id) {
                    this.set({'isLoggedIn': true, 'silent': true});
                }
                this.on('change:google', this.onChange, this);
            },
            onChange: function (modelChanged) {
                this.set({'isLoggedIn': !!modelChanged.get('google').id});
            },
            logout: function () {
                var that = this;
                this.destroy(
                    {
                        success: function () {
                            that.set({'isLoggedIn': false,'_id':0});
                        }
                    }
                );
            },
            blankLandmark: undefined,
            getActiveLandmark: function () {
                var id = this.get('selectedLandmarkId');
                if (!State.landmarks) {
                    return undefined;
                }
                if (!id && State.landmarks && State.landmarks.length > 0) {
                    var firstLandmark = State.landmarks.first();
                    this.set({'selectedLandmarkId': firstLandmark.get('_id')});
                    return firstLandmark;
                }
                var landmark = State.landmarks.findWhere({'_id': id});
                if (!landmark || typeof(landmark) === 'undefined') {

                    if (!this.blankLandmark) {
                        this.blankLandmark = new LandmarkModel();
                    }
                    return this.blankLandmark;
                }
                return landmark;
            }
        });
    });