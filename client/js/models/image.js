define([

],
function(){
    return Backbone.Model.extend({
        defaults:{
            name:'',
            id:-1,
            landmarkId:-1,
            url:''
        },
        initialize:function(){

        }
    });
});