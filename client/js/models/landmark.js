define(['jquery', 'underscore', 'backbone'],
    function ($, _, Backbone) {
        return Backbone.Model.extend({
            url: function () {
                return '/api/landmark';
            },
            idAttribute: '_id',
            defaults: {
                _id:undefined,
                name: '',
                description: '',
                thumbnailUrl: '',
                created_date: undefined
            },
            initialize: function () {
            }
        });
    }
)
;