define([
        'state',
        'mustache',
        'text!templates/new-modal.html'
    ],
    function (State, Mustache, ModalTemplate) {
        return Backbone.View.extend({
            events: {
                'click .js-new-landmark-save-btn': 'onSaveClick',
                'click .js-new-landmark-close-btn': 'onCloseClick'
            },
            initialize: function () {
                this.render();
                this.listenTo(State.session, 'change:isLoggedIn', this.render);
            },
            onSaveClick: function (evt) {
                evt.preventDefault();
                var that = this;
                State.landmarks.create({
                        name:this.$el.find('.js-name-txt').val(),
                        description:this.$el.find('.js-description-txt').val(),
                        thumbnailUrl:this.$el.find('.js-thumbnail-txt').val()
                    },
                    {
                        wait:true,
                        success:function(){
                            this.onCloseClick({});
                        },
                        error:function(){
                            // TODO: handle error in creating new landmark
                            this.onCloseClick({});
                        }
                    }
                );
            },
            onCloseClick: function (evt) {
                // Clear form first
                this.$el.find('.js-thumbnail-txt, .js-description-txt, .js-name-txt').val('');
                that.$el.find('#new-landmark-modal').modal('hide');
            },
            isRendered: false,
            render: function () {
                if (!this.isRendered) {
                    var model = {};
                    var html = Mustache.render(ModalTemplate, model);
                    this.$el.html(html);
                    this.isRendered = true;
                }

                var isLoggedIn = State.session.get('isLoggedIn');
                var modalButton = this.$el.find('.js-create-modal-btn');
                if (isLoggedIn) {
                    modalButton.removeClass('hide-until-render');
                } else {
                    modalButton.addClass('hide-until-render');
                }
            }
        });
    }
);