define([
        'state',
        'mustache',
        'text!templates/details-pics.html'
],
function(State,Mustache,PicTemplate){
   return Backbone.View.extend({
      initialize:function(){
          this.render();
      },
       render:function(){
           var model = {
               pics:State.images.toJSON()
           };
           var html = Mustache.render(PicTemplate,model);
           this.$el.html(html);
       }
   });
});