define([
        'state',
        'mustache',
        'text!templates/navbar.html',
        'login',
        'parentView',
        'views/createLandmark'
    ],
    function(State,Mustache,NavTemplate,LoginView,ParentView,CreateNew){
        return ParentView.extend({
            initialize:function(){
                this.render();
            },
            loginView:[],
            render:function(){
                var html = Mustache.render(NavTemplate,{});
                this.$el.html(html);

                var createLandmarkHolder = this.$el.find('.js-create-holder');
                this.childViews.createView = new CreateNew({'el':createLandmarkHolder});

                var loginHolder = this.$el.find('.js-login-holder');
                this.childViews.loginView = new LoginView({'el':loginHolder});
            }
        });
    }
);