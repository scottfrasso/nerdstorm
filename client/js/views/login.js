define([
        'state',
        'mustache',
        'text!templates/login.html'
    ],
    function (State, Mustache, LoginTemplate) {
        return Backbone.View.extend({
            events: {
                'click .js-login-btn': 'onLoginClick',
                'click .js-logout-btn': 'onLogoutClick'
            },
            initialize: function () {
                this.render();
                this.listenTo(State.session,'change:isLoggedIn',this.isLoggedInChange);
                this.listenTo(State.session,'change:username',this.onUserNameChanged);
            },
            onLoginClick: function (evt) {
                evt.preventDefault();
                var username = this.usernameTextBox.val();
                var password = this.passwordTextBox.val();
                State.session.login(username,password);
            },
            onLogoutClick: function (evt) {
                evt.preventDefault();
                State.session.logout();
            },
            isRendered:false,
            loginButton:[],
            loginForm:[],
            usernameLabel:[],
            logOutButton:[],
            usernameTextBox:[],
            passwordTextBox:[],
            onUserNameChanged:function(modelChanged){
                this.usernameLabel.val('Welcome '+ modelChanged.get('username'));
                this.render();
            },
            isLoggedInChange:function(){
                this.passwordTextBox.val('');
                this.render();
            },
            render: function () {
                if (!this.isRendered) {
                    var html = Mustache.render(LoginTemplate, {});
                    this.$el.html(html);
                    this.loginForm = this.$el.find('.js-login-form');
                    this.loginButton = this.$el.find('.js-login-btn');
                    this.logOutButton = this.$el.find('.js-logout-btn');
                    this.usernameLabel = this.$el.find('.js-username-label');
                    this.usernameTextBox = this.$el.find('.js-username-text-box');
                    this.passwordTextBox = this.$el.find('.js-password-text-box');
                    this.isRendered = true;
                }
                // Username
                var username = State.session.get('name');
                this.usernameLabel.html('Welcome ' +username);
                // Logged in
                var isLoggedIn = State.session.get('isLoggedIn');
                if (isLoggedIn) {
                    this.loginForm.addClass('hidden');
                    this.usernameLabel.removeClass('hidden');
                    this.logOutButton.removeClass('hidden');
                } else {
                    this.loginForm.removeClass('hidden');
                    this.usernameLabel.addClass('hidden');
                    this.logOutButton.addClass('hidden');
                }
            }
        });
    }
);