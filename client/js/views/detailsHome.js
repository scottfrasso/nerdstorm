define([
        'state',
        'mustache',
        'parentView',
        'picsView',
        'jumboView',
        'text!templates/details-home.html'
    ],
    function (State, Mustache,ParentView, PicsView, JumboView, DetailsHomeTemplate) {
        return ParentView.extend({
            initialize: function () {
                this.listenTo(State.session, 'change:selectedLandmarkId', this.render);
            },
            isRendered: false,
            render: function () {
                if (!this.isRendered) {
                    var html = Mustache.render(DetailsHomeTemplate, {});
                    this.$el = $(html);
                }
                var landmark = State.session.getActiveLandmark();
                var nav = {
                    'navs': [
                        {
                            'link': 'search/' + landmark.get('id'),
                            'name': 'Search Landmarks'
                        }
                    ],
                    'name': landmark.get('name')
                };
                State.vent.trigger(State.SetNavEvent, nav);

                // Child views
                if (!this.isRendered) {
                    this.isRendered = true;
                    var jumboElement = this.$el.find('.js-details-jumbotron');
                    this.childViews.jumboView = new JumboView({'el': jumboElement});

                    var element = this.$el.find('.js-pics-bucket');
                    this.childViews.picsView = new PicsView({'el': element});
                }
                this.childViews.jumboView.render();
                this.childViews.picsView.render();

                return this;
            }
        });
    }
);