define([
        'state',
        'mustache',
        'text!templates/bread-crumbs.html'
],
function(State,Mustache,BreadcrumbsTemplate){
    return Backbone.View.extend({
        initialize:function(){
            State.vent.on(State.SetNavEvent,this.onSetNav,this);
        },
        onSetNav:function(navModel){
            var model = _.extend({
                navs : [],
                name : ''
            },navModel);
            var html = Mustache.render(BreadcrumbsTemplate,model);
            this.$el.html(html);
        },
        render:function(){
            this.$el.html('');
        }
    });
});