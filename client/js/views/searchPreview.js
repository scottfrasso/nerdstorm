define([
        'state',
        'mustache',
        'text!templates/search-preview.html'
],
function(State, Mustache,PreviewTemplate){
    return Backbone.View.extend({
        initialize:function(){
            this.listenTo(State.session,'change:selectedLandmarkId',this.onActiveChanged);
        },
        onActiveChanged:function(){
            this.render();
        },
        render:function(){
            var landmark = State.session.getActiveLandmark();
            var previewHtml = '';
            if (landmark.get('_id') !== undefined){
                previewHtml = Mustache.render(PreviewTemplate,landmark.toJSON());
            }else{

                previewHtml = Mustache.render(PreviewTemplate,landmark.toJSON());
            }
            this.$el.html(previewHtml);
        }
    });
});