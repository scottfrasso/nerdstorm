define([
        'state',
        'mustache',
        'text!templates/search-results.html'
    ],
    function (State, Mustache, ResultsTemplate) {
        return Backbone.View.extend({
            initialize: function () {
                this.render();
                this.listenTo(State.session, 'change:selectedLandmarkId', this.onSelectedLandmarkChange);
                this.listenTo(State.landmarks, 'sync', this.render);
            },
            onSelectedLandmarkChange: function (modelChanged) {
                var activeId = modelChanged.get('selectedLandmarkId');
                this.renderActiveRecord(activeId);
            },
            renderActiveRecord:function(activeId){
                this.$el.find('.js-search-item').removeClass('active');
                if (typeof(activeId) === 'undefined'){
                    return null;
                }
                this.$el.find('.js-search-item[data-id=' + activeId + ']').addClass('active');
            },
            render: function () {
                var items = this.$el.find('.js-search-item, .js-search-message');
                if (items.length > 0) {
                    items.remove();
                }
                var resultList = {
                    messages: [],
                    results: State.landmarks.toJSON()
                };
                if (resultList.results.length === 0) {
                    resultList.messages.push({message: 'Sorry no results were found.'});
                }
                var output = Mustache.render(ResultsTemplate, resultList);
                this.$el.find('.js-search-message').remove();
                this.$el.find('.js-search-results').remove();
                this.$el.append(output);

                if (State.landmarks.length > 0){
                    var activeId = State.landmarks.first().get('_id');
                    this.renderActiveRecord(activeId);
                }
            }
        });
    }
);