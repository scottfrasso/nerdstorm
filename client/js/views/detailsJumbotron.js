define([
        'state',
        'mustache',
        'text!templates/details-jumbotron.html',
        'text!templates/new-image-popover.html'
    ],
    function (State, Mustache, JumboTemplate, NewImagePopoverTemplate) {
        return Backbone.View.extend({
            initialize: function () {
                this.render();
            },
            isRendered: false,
            popOverAnchor: undefined,
            onNewImageClick: function () {
                debugger;
            },
            render: function () {
                var landmark = State.session.getActiveLandmark();
                var model = {
                    landmark: landmark.toJSON(),
                    popoverContent:NewImagePopoverTemplate
                };
                var html = Mustache.render(JumboTemplate, model);
                this.$el.html(html);

                if (!this.isRendered) {
                    this.$el.find('#new-image-btn').popover();
                    /*
                    this.isRendered = true;

                    this.popOverAnchor = this.$el.find('#new-image-btn').popover({trigger:'click manual',html: true,placement: 'right',title:'New Image',template: '<div>Hello World</div>'});
                    //ar parent = imagePopover.parent();
                    this.popOverAnchor.popover("show");
                    //parent.delegate('button#new-image-create-btn', 'click', this.onNewImageClick);
                    //this.popOverAnchor.popover('show');*/
                }
            }
        });
    });