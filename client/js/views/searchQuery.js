define([
        'state',
        'mustache',
        'text!templates/search-query.html'
    ],
    function (State, Mustache, QueryTemplate) {
        return Backbone.View.extend({
            events: {
                'click .js-search-btn': 'onQueryClick'
            },
            onQueryClick: function (evt) {
                evt.preventDefault();
                var searchText = this.$el.find('.js-search-input').val();
                this.requestSearch(searchText);
            },
            requestSearch:function(searchText){
                var params = {};
                if (searchText && searchText.length > 0){
                    params.q = searchText;
                }
                State.landmarks.fetch({data:params});
            },
            initialize: function () {
                this.listenTo(State.landmarks,'request',this.onRequestStart);
                this.listenTo(State.landmarks,'error',this.onSearchError);
                this.listenTo(State.landmarks,'sync',this.onSearchComplete);
            },
            onRequestStart:function(){
                // Set to "Loading..."
                State.session.set({'selectedLandmarkId':undefined,'silent':true});
                this.setControlsEnabled(false);
            },
            onSearchError: function(){
                // set error
                this.setControlsEnabled(true);
            },
            onSearchComplete:function(){
                this.setControlsEnabled(true);
                // Set the active landmark to the first
                if (State.landmarks.length === 0){
                    return null;
                }
                var activeId = State.landmarks.first().get('id');
                State.session.set({'selectedLandmarkId':activeId});
            },
            setControlsEnabled:function(setToEnabled){
                this.$el.find('.js-search-input, .js-search-btn').prop('disabled',!setToEnabled);
            },
            render: function () {
                var html = Mustache.render(QueryTemplate, {});
                this.$el.html(html);
            }
        });
    }
);