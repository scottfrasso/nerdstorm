define([
        'state',
        'mustache',
        'parentView',
        'text!templates/search-home.html',
        'searchResults',
        'searchPreview',
        'queryView'
    ],
    function (State, Mustache, ParentView, SearchHomeTemplate, SearchResults, SearchPreview,QueryView) {
        return ParentView.extend({
            initialize: function () {
                this.listenTo(State.session,'change:selectedLandmarkId',this.onSelectedLandmarkChange);
            },
            onSelectedLandmarkChange:function(modelChanged){
                var activeId = modelChanged.get('selectedLandmarkId');
                State.vent.trigger(State.RouteChange, {'nav': 'search/' + activeId});
            },
            isRendered: false,
            render: function () {
                if (!this.isRendered) {
                    this.isRendered = true;
                    var html = Mustache.render(SearchHomeTemplate, {});
                    this.$el = $(html);

                    var previewElement = this.$el.find('.js-search-preview');
                    this.childViews.searchPreview = new SearchPreview({'el': previewElement});

                    var queryElement = this.$el.find('.js-query-holder');
                    this.childViews.queryView = new QueryView({'el':queryElement});

                    var resultsElement = this.$el.find('.js-results-holder');
                    this.childViews.resultsView = new SearchResults({'el': resultsElement})
                }
                // Render child views
                this.childViews.queryView.render();
                this.childViews.searchPreview.render();

                State.vent.trigger(State.SetNavEvent, {name: 'Search Landmarks'});
                return this;
            }
        });
    }
);
