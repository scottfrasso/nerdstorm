define('config', function (config) {
    require.config({
        enforceDefine: false,
        baseUrl: 'js/',
        paths: {
            // Libraries
            jquery: 'node_modules/jquery/dist/jquery.min',
            underscore: 'node_modules/underscore/underscore-min',
            backbone: 'node_modules/backbone/backbone',

            bootstrap: './node_modules/bootstrap-sass/assets/javascripts/bootstrap',
            popover:'./node_modules/bootstrap-sass/assets/javascripts/bootstrap/popover',
            json: './node_modules/JSON2/json2',
            mustache: './node_modules/mustache/mustache',

            // Utils
            state: './util/state',
            events: './util/events',
            storage: './util/storage',
            parentView: './util/ParentView',

            // Models / Collections
            images: './collections/images',
            session: './models/session',
            image: './models/image',

            landmark: './models/landmark',
            landmarks: './collections/landmarks',

            // Views
            navbar: './views/navbar',
            login: './views/login',
            breadcrumbs: './views/breadcrumbsView',

            searchHome: './views/searchHome',
            queryView: './views/searchQuery',
            searchResults: './views/searchResults',
            searchPreview: './views/searchPreview',

            detailsHome: './views/detailsHome',
            picsView: './views/detailPics',
            jumboView: './views/detailsJumbotron',

            router: './src/router'
        },
        shim: {
            bootstrap: {
                deps: ['jquery']//,
                //exports: '$.fn.popover'
            },
            underscore: {
                exports: '_'
            },
            backbone: {
                deps: ['underscore', 'jquery'],
                exports: 'Backbone'
            },
            popover:{
                deps:['bootstrap'],
                exports:'$.fn.popover'
            }
        }
    });
});
