define([
    'image'
],
function(ImageModel){
    return Backbone.Collection.extend({
        model:ImageModel
    });
});