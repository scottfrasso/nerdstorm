define([
    'landmark'
],
function(Landmark){
   return Backbone.Collection.extend({
       model:Landmark,
       url:'/api/landmarks'
   });
});