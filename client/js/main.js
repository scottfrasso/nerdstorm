require([
        'config'
    ],
    function (opts) {
        // Config loaded
        console.log('config loaded');
        require([
                'jquery', 'underscore', 'bootstrap', 'backbone','popover'
            ],
            function ($, Bootstrap, _, Backbone) {
                // jQuery and libraries loaded
                console.log('js libs loaded');
                require(['app'],
                    function (App) {
                        console.log('app loaded');
                        App.load(opts);
                    }
                );
            });
    }
);