define([
        'jquery','underscore','backbone',
    'state'
],
function($,_,Backbone,State){
    return Backbone.Router.extend({
        routes:{
            'landmark/:id':'getLandmark',
            'search/:id':'selectedLandmark',
            '*actions':'defaultRoute'
        },
        initialize:function(){
        },
        getLandmark:function(id){
            if (!id){
                return;
            }
            State.session.set({'selectedLandmarkId':id});
            State.vent.trigger(State.ShowDetails);
        },
        selectedLandmark:function(id){
            if (!id){
                return;
            }
            State.session.set({'selectedLandmarkId':id});
            State.vent.trigger(State.ShowSearch);
        },
        defaultRoute:function(){
            // Show the default
            State.session.set({'selectedLandmarkId':undefined});
            State.vent.trigger(State.ShowSearch);
        },
        start:function(){
            Backbone.history.start();
        }
    });
});