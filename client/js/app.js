define([
        'state',
        'events',
        'router',
        'searchHome',
        'detailsHome',
        'landmarks',
        'session',
        'breadcrumbs',
        'images',
        'navbar',
        'mocks/mockImages'
    ],
    function (State, Events, Router, SearchHome, DetailsHome,
              LandmarkList, Session, BreadCrumbs, ImageList, NavBar,
              MockImages) {
        return {
            defaults:{
                vent: _.extend({}, Backbone.Events)
            },
            loadView: function (viewClass) {
                if (this.currentViewClass && this.currentViewClass !== viewClass) {
                    this.view.close();
                    this.view = undefined;
                }
                this.currentViewClass = viewClass;
                if (!this.view) {
                    this.view = new viewClass();
                    $('.js-landmark-app').append(this.view.render().$el);
                }
            },
            onShowSearch: function () {
                this.loadView(SearchHome);
            },
            onShowDetails: function () {
                this.loadView(DetailsHome);
            },
            onRouteChange:function(routeChangeEvent){
                if (!State.router){
                    return;
                }
                State.router.navigate(routeChangeEvent.nav);
            },
            load: function (config) {
                var opts = _.extend(this.defaults, config);
                State = _.extend(State, opts);
                State = _.extend(State, Events);

                // Load the bootstrapped values
                if (NDS && NDS.userSessions && NDS.userSessions.length == 1){
                    var userModel = NDS.userSessions[0];
                    State.session = new Session(userModel);
                }
                if (!State.session){
                    // This should never happen, but just in case
                    State.session = new Session();
                }

                if (NDS && NDS.landmarkList && NDS.landmarkList.length > 0){
                    State.landmarks = new LandmarkList();
                    State.landmarks.reset(NDS.landmarkList);
                    //State.landmarks = new LandmarkList(NDS.landmarkList.models);
                }
                if (!State.landmarks){
                    // This should never happen, but just in case
                    State.landmarks = new LandmarkList();
                }

                State.vent.on(State.RouteChange,this.onRouteChange,this);
                State.router = new Router();

                State.navbar = new NavBar({'el': $('.js-navbar-holder')});
                State.breadcrumbs = new BreadCrumbs({'el': $('.js-bread-crumbs-holder')});

                State.vent.on(State.ShowDetails, this.onShowDetails, this);
                State.vent.on(State.ShowSearch, this.onShowSearch, this);

                State.images = new ImageList(MockImages.images);
                State.router.start();

                window.State = State;
            }
        }
    }
);