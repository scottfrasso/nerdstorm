var _ = require('underscore');
var mongoose = require('mongoose');
//var textSearch = require('mongoose-text-search');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

// Landmark
var landmarkSchema = mongoose.Schema({
    name: String,
    description: String,
    thumbnailUrl: String,
    created_date: Date,
    images:[{type:Schema.Types.ObjectId,ref:'Image'}]
});

landmarkSchema.statics.searchQueryString = function (options, callback) {
    var _defaultOptions = {query: '', page: 0, pageSize: 10};
    options = _.extend(_defaultOptions, options);
    //TODO: mongolabs will add 2.6 (with Text Search) on Nov 19th
    var queryText = options.query || '';
    this.find()
        .where()
        .skip(options.page * options.pageSize)
        .limit(options.pageSize)
        .exec(callback);
};

// Images
var imageSchema = mongoose.Schema({
    landmark:{type:Schema.Types.ObjectId,ref:'Landmark'},
    name: String,
    originalUrl: String,
    created_date: Date
});

module.exports = exports = {
    'Landmark': mongoose.model('Landmark', landmarkSchema),
    'Image':mongoose.model('Image',imageSchema)
};