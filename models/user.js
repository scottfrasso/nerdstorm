var mongoose = require('mongoose');
var Chance = require('chance');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var userSchema = mongoose.Schema({
    user_id: ObjectId,
    name: String,
    isPowerUser:{type:Boolean,default:false},
    facebook: {
        id: String,
        name: String,
        email: String
    },
    google: {
        id: String,
        name: String,
        email: String
    },
    last_seen: Date
});

userSchema.methods.applyProfile = function (profileType, profile) {
    if (!this[profileType]) {
        this[profileType] = {id:'',name:'',email:''};
    }

    if (!this.name){
        this.name = Chance.name({middle_initial:true});
    }

    this[profileType].id = profile.id;
    this[profileType].name = profile.displayName;
    this[profileType].email = profile.email;
    this.last_seen = new Date();
};

userSchema.static.getIsPowerUser = function(userId,callback){
    User.findOne({'_id':userId},function(err,userResult){
            if (!err) return callback(err);
            return callback(err,userResult.isPowerUser);
        }
    );
};

module.exports = exports = mongoose.model('User', userSchema);