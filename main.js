var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var configAuth = require('./config/auth');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var methodOverride = require('method-override');

// Mongoose
var mongoose = require('mongoose');
var uristring = configAuth.mongo;
mongoose.connect(uristring, function (err, res) {
    if (err) {
        console.log('ERROR connecting to: ' + uristring + '. ' + err);
    } else {
        console.log('Succeeded connected to: ' + uristring);
    }
});
// Load models
require('./models/user');
require('./models/landmark');

app.get(function(err,req,res,next){
   if (!err) return next();
    console.log('error!',err);
    res.send('Sorry there has been an error');
});

// index.html is loaded by index.ejs
app.set('views', path.resolve('./client'));
app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(session({
            secret: 'keyboard cat',
            store: new RedisStore({
                    host: configAuth.redisToGo.host,
                    port: configAuth.redisToGo.port,
                    user: configAuth.redisToGo.user,
                    pass: configAuth.redisToGo.pass
                }
            )
        }
    )
);
// Passport
app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverride());

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

require('./src/app-routes.js')(express, app, passport);

// Static client
var initialLoad = require('./src/initial-load');
app.get('/',initialLoad);
var staticAssetOpts = {
    dotfiles:'ignore',
    etag:false,
    extensions:false,
    index:false,
    'setHeaders':function(res){
        res.setHeader('Cache-Control','max-age=290304000, no-transform, public');
    }
};

app.get(express.static(path.resolve('./client/static'),clientStaticOptions));

var clientStaticOptions = {
    dotfiles:'ignore',
    etag:false,
    extensions:false,
    index:false,
    'setHeaders':function(res){
        if (process.env.NOCACHE && process.env.NOCACHE === 'true'){
            // for the dev enviroment don't cache anything
            res.setHeader('Cache-Control','no-store, no-cache');
        }
    }
};
app.use(express.static(path.resolve('./client'),staticAssetOpts));
var port = process.env.PORT || 8080;
app.listen(port);
console.log('Server listening on port = ' + port);