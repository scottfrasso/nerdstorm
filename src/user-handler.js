var mongoose = require('mongoose');
var User = mongoose.model('User');

function UserHandler() {

}

// POST
UserHandler.prototype.create = function (req, res) {
    console.log('noop', req.query);
    res.json({'noop': true});
};

// GET
UserHandler.prototype.read = function (req, res) {
    console.log('noop', req.query);
    res.json({'noop': true});
};

// PUT
UserHandler.prototype.update = function (req, res) {
    console.log('noop', req.query);
    res.json({'noop': true});
};

// DELETE
UserHandler.prototype.delete = function (req, res) {
    req.logOut();
    res.json({});
};

module.exports = exports = new UserHandler();