var _ = require('underscore');
var JSON2 = require('JSON2');
var UserModel = require('../models/user');
var mongoose = require('mongoose');
var models = require('../models/landmark');
var LandmarkModel = models.Landmark;

var async = require('async');

var configAuth = require('../config/auth');
var redis = require('redis');

var redisClient = redis.createClient(parseInt(configAuth.redisToGo.port),configAuth.redisToGo.host);
redisClient.on('error', function (err) {
    console.error(err);
});
redisClient.auth(configAuth.redisToGo.pass);

var currentBuildCount = 0;

var _getServerCount = function (req, res,callback) {
    redisClient.get('build-counter', function (err, reply) {
        if (err) {
            console.error('error:', err);
            var buildNum = 'e' + currentBuildCount;
            return callback(null, buildNum);
        }
        currentBuildCount = reply;
        callback(null, reply);
    });
};

var _loadUser = function (req, res, callback) {
    if (req.isAuthenticated()) {
        UserModel.findOne({'user_id': req.user_id}, function (err, user) {
                if (err) return callback(err, null);
                if (user) {
                    callback(null, user.toJSON());
                } else {
                    callback(null, {});
                }
            }
        );
    } else {
        callback(null, {});
    }
};

var _loadLandmarks = function (req, res, callback) {
    LandmarkModel.searchQueryString({},
        function (err, landmarkList) {
            if (err) return callback(err, null);
            var landmarkArr = _.map(landmarkList, function (item) {
                return item.toJSON();
            });
            callback(null, landmarkArr);
        }
    )
};

var initialLoad = function (req, res) {
    console.time('load-time');
    async.parallel([
            function (callback) {
                _getServerCount(req,res,callback);
            },
            function (callback) {
                _loadUser(req, res, callback);
            },
            function (callback) {
                _loadLandmarks(req, res, callback);
            }
        ],
        function (err, results) {
            if (err){
                console.error('Could not perform initial load',err);
                return res.render('Sorry could not load website :-(');
            }

            var buildCount = results[0];
            var initialUserSession = results[1];
            if (!initialUserSession.searchText) {
                initialUserSession.searchText = '';
            }
            var model = {
                userSessions: JSON2.stringify([initialUserSession]),
                landmarkResults: JSON2.stringify(results[2]),
                cssFile: process.env.cssFile || 'static/style.css',
                devEnviroment: (process.env.DEV && process.env.DEV === 'true'),
                staticAssetPath: process.env.STATIC_PATH || 'static/',
                cacheBuster: process.env.CACHE_BUSTER || buildCount
            };
            // Don't cache the Home Page because it has bootstrapped data (index.html)
            res.setHeader('Cache-Control', 'no-store, no-cache');
            res.render('index.ejs', model);
            console.timeEnd('load-time');
        }
    );
};

module.exports = exports = initialLoad;