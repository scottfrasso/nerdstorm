var _ = require('underscore');
var mongoose = require('mongoose');
var Landmark = mongoose.model('Landmark');
var User = mongoose.model('User');

// TODO: need to hookup database
var _mockLandmarkList = [
    {
        id: 1,
        name: 'scott landmark',
        description: 'landmark description',
        thumbnailUrl: 'http://s.hswstatic.com/gif/arc-de-triomphe-landmark-1.jpg'
    },
    {
        id: 2,
        name: 'bob landmark',
        description: 'landmark description',
        thumbnailUrl: 'http://s.hswstatic.com/gif/arc-de-triomphe-landmark-1.jpg'
    },
    {
        id: 3,
        name: 'jill landmark',
        description: 'landmark description',
        thumbnailUrl: 'http://s.hswstatic.com/gif/arc-de-triomphe-landmark-1.jpg'
    }
];

function Landmarks() {
}

Landmarks.prototype.search = function (req, res) {
    console.log('search', req.query);
    var searchText = req.query['q'] ? req.query['q'] : '';

    var _searchCallback = function (err, landmarks) {
        if (err) return res.json({'error': true});
        res.json(landmarks);
    };

    Landmark.searchQueryString({'query':searchText},_searchCallback);
};

Landmarks.prototype.create = function (req, res) {
    console.log('create', req.body);

    // TODO: this code should be DRY
    if (!req.isAuthenticated()){
        res.statusCode = 401;
        return res.json({'error':'unauthorized'});
    }

    var obj = req.body;
    var landmark = new Landmark({
            name: obj.name,
            description:obj.description,
            thumbnailUrl: obj.thumbnailUrl,
            created_date: new Date()
        }
    );

    landmark.save(function (err,landmarkModel) {
            if (err){
                console.error('Error creating landmark:',obj,' ',err);
                return res.json({'error': true});
            }
            return res.json(landmarkModel.toJSON());
        }
    );
};

Landmarks.prototype.read = function (req, res) {
    console.log('read', req.query);
    var selectedId = req.params.id;

    Landmark.findOne({'_id':selectedId},
    function (err,landmarkModel){
        if (err){
            console.error('Error reading landmark id:',selectedId,err);
            return res.json({'error':true});
        }
        return res.json(landmarkModel.toJSON());
    });
};

Landmarks.prototype.edit = function (req, res) {
    console.log('edit', req.query);
    var selectedId = req.params.id || req.body._id ;

    // TODO: this code should be DRY
    if (!req.isAuthenticated()){
        res.statusCode = 401;
        return res.json({'error':'unauthorized'});
    }

    var model = {
        'name':req.body.name || '',
        'description': req.body.description || '',
        'thumbnailUrl':req.body.thumbnailUrl || ''
    };

    Landmark.findOneAndUpdate(
        {'_id':selectedId},
        model,
        {},
        function(err,landmarkModel){
            if (err) {
                console.error('Error Editing landmark:',model,err);
                return res.json({'error':true});
            }
            res.json(landmarkModel);
        }
    );
};

Landmarks.prototype.delete = function (req, res) {
    console.log('delete', req.query);
    res.json({});
};

Landmarks.prototype.noop = function (req, res) {
    console.log('noop', req.query);
    res.json({'noop': true});
};

// singleton
module.exports = exports = new Landmarks();