var landmark = require('./landmark-handler');
var user = require('./user-handler');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
//var FacebookStrategy = require('passport-facebook');
var configAuth = require('../config/auth');
var User = require('../models/user');

var logroute = function (message) {
    return function (req, res, next) {
        if (process.env.LOG_API_CALLS && process.env.LOG_API_CALLS === 'true'){
            console.log(message, req.method, req.url, req.query, ' -sessionID:', req.sessionID, ' -sessionKey:');
        }
        next();
    };
};

module.exports = function (express, app, passport) {
    var apiRouter = express.Router();
    apiRouter.use(logroute('API-ROUTE'));
    // Landmark Collection
    apiRouter.all('/landmarks/:q?', landmark.search);
    // Landmark Model
    apiRouter.get('/landmark/:id', landmark.read);
    apiRouter.post('/landmark/', landmark.create);
    apiRouter.put('/landmark/:id', landmark.edit);
    apiRouter.delete('/landmark/:id', landmark.delete);

    // User - Session
    apiRouter.post('/user', user.create);
    apiRouter.get('/user', user.read);
    apiRouter.put('/user', user.update);
    apiRouter.delete('/user', user.delete);
    app.use('/api', apiRouter);

    // OAuth
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (obj, done) {
        done(null, obj);
    });
/*
    passport.use(new FacebookStrategy({
                clientID: configAuth.facebookAuth.clientID,
                clientSecret: configAuth.facebookAuth.clientSecret,
                callbackURL: configAuth.facebookAuth.callbackURL,
                enableProof: false,
                profileFields: ['id', 'displayName', 'photos']
            },
            function (accessToken, refreshToken, profile, done) {
                User.findOne({'facebook.id': profile.id}), function (err, user) {
                    if (err) {
                        return done(err);
                    }
                    if (user) {
                        user.applyProfile('facebook',profile);
                        user.save(function (err) {
                                if (err) {
                                    return done(err);
                                }
                                return done(null, user);
                            }
                        );
                    } else {
                        var newUser = new User();
                        newUser.applyProfile('facebook',profile);
                        newUser.save(function (err) {
                            if (err) {
                                return done(err);
                            }
                            return done(null, newUser);
                        });
                    }
                }
            }
        )
    );*/

    var authRouter = express.Router();
    authRouter.use(function (req, res, next) {
        logroute('API-ROUTE');
        next();
    });

    app.get('/logout',function(req,res,next){
        req.logout();
        res.redirect('/');
    });
    /*
    authRouter.get('/facebook',
        passport.authenticate('facebook'));

    authRouter.get('/facebook/callback',
        passport.authenticate('facebook', {failureRedirect: '/'}),
        function (req, res) {
            // Successful authentication, redirect home.
            res.redirect('/');
        }
    ); */

    passport.use(new GoogleStrategy({
                clientID: configAuth.googleAuth.clientID,
                clientSecret: configAuth.googleAuth.clientSecret,
                callbackURL: configAuth.googleAuth.callbackURL
            },
            function (token, refreshToken, profile, done) {
                process.nextTick(function () {
                    User.findOne({'google.id': profile.id}, function (err, user) {
                            if (err) {
                                return done(err);
                            }
                            if (user) {
                                user.applyProfile('google',profile);
                                user.save(function (err) {
                                    if (err) {
                                        return done(err);
                                    }
                                    return done(null, user);
                                });
                            } else {
                                var newUser = new User();
                                newUser.applyProfile('google',profile);
                                newUser.save(function (err) {
                                    if (err) {
                                        return done(err);
                                    }
                                    return done(null, newUser);
                                });
                            }
                        }
                    );
                });
            })
    );

    authRouter.get('/google', passport.authenticate('google', {scope: ['profile', 'email']}));

    authRouter.get('/google/callback',
        passport.authenticate('google', {
            successRedirect: '/',
            failureRedirect: '/auth/google/failed' // TODO: need to make this handle this case
        }));
    authRouter.all('/google/failed', logroute('Failed to authenticate with Google'));
    app.use('/auth', authRouter);
};