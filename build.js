var requirejs = require('requirejs');
var path = require('path');
var fs = require('fs');

var configAuth = require(path.resolve('config/auth'));
var redis = require('redis');

// increment the build number
var redisClient = redis.createClient(parseInt(configAuth.redisToGo.port), configAuth.redisToGo.host);
redisClient.on('error', function (err) {
    if (err)console.error(err);
    redisClient.end();
});
redisClient.auth(configAuth.redisToGo.pass);

redisClient.incr('build-counter', function (err) {
    if (err) console.error(err);
    console.log('redis - build-counter incremented');
    redisClient.end();
});

var config = {
    baseUrl: 'client/js',
    name: 'main',
    out: 'client/static/main.min.js',
    mainConfigFile: 'client/js/config.js',
    optimize: 'uglify',
    findNestedDependencies: true
};

requirejs.optimize(config, function () {
    console.log('Optimization finished');
    var fileRequirejs = path.resolve('client/js/node_modules/requirejs/require.js');
    var copyRequirejs = path.resolve('client/static/require.js');

    // move a copy of requirejs to the static directory so it can be cached
    fs.createReadStream(fileRequirejs)
        .pipe(fs.createWriteStream(copyRequirejs));
}, function (err) {
    //optimization err callback
    console.error(err);
    console.error('optimization failed');
});

var sass = require('node-sass');
sass.render({
    file: path.resolve('client/sass/style.scss'),
    success: function (result) {
        fs.writeFile(path.resolve('client/static/style.css'), result, {}, function (err) {
            if (err) return console.error(err);
            console.log('sass finished');
        });
    },
    error: function (err) {
        console.error(err);
        console.error('could not compile sass');
    },
    outputStyle: 'compressed'
});