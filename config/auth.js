var fs = require('fs');
var JSON2 = require('JSON2');
var path = require('path');

var authCredentials = {};
if (process.env.auth){
    authCredentials = JSON2.parse(process.env.auth);
    console.log('loaded prod credentials');
}
var hiddenFilePath = path.join(__dirname,'/auth-hidden.js');
if (fs.existsSync(hiddenFilePath)){
    authCredentials = require('./auth-hidden');
    console.log('loaded dev credentials');
}

module.exports = authCredentials;